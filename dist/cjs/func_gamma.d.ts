export declare function gammaln(x: number): number;
export declare function loggam(x: number): number;
export declare function gammafn(x: number): number;
export declare function gammap(a: number, x: number): number;
export declare function lowRegGamma(a: number, x: number): number;
export declare function gammapinv(p: number, a: number): number;
