export declare function betafn(x: number, y: number): number | undefined;
export declare function betaln(x: number, y: number): number;
export declare function betacf(x: number, a: number, b: number): number;
export declare function ibetainv(p: number, a: number, b: number): number;
export declare function ibeta(x: number, a: number, b: number): number;
