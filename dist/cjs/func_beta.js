"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ibeta = exports.ibetainv = exports.betacf = exports.betaln = exports.betafn = void 0;
const func_gamma_js_1 = require("./func_gamma.js");
// beta function
function betafn(x, y) {
    // ensure arguments are positive
    if (x <= 0 || y <= 0) {
        return undefined;
    }
    else {
        return Math.exp((0, func_gamma_js_1.gammaln)(x) + (0, func_gamma_js_1.gammaln)(y) - (0, func_gamma_js_1.gammaln)(x + y));
    }
}
exports.betafn = betafn;
;
// natural logarithm of beta function
function betaln(x, y) {
    return (0, func_gamma_js_1.gammaln)(x) + (0, func_gamma_js_1.gammaln)(y) - (0, func_gamma_js_1.gammaln)(x + y);
}
exports.betaln = betaln;
;
// Evaluates the continued fraction for incomplete beta function
// by modified Lentz's method.
function betacf(x, a, b) {
    let fpmin = 1e-30;
    let qab = a + b;
    let qap = a + 1;
    let qam = a - 1;
    let c = 1;
    let d = 1 - qab * x / qap;
    // These q's will be used in factors that occur in the coefficients
    if (Math.abs(d) < fpmin) {
        d = fpmin;
    }
    d = 1 / d;
    let h = d;
    for (let m = 1; m <= 100; m++) {
        let m2 = 2 * m;
        let aa = m * (b - m) * x / ((qam + m2) * (a + m2));
        // One step (the even one) of the recurrence
        d = 1 + aa * d;
        if (Math.abs(d) < fpmin) {
            d = fpmin;
        }
        c = 1 + aa / c;
        if (Math.abs(c) < fpmin) {
            c = fpmin;
        }
        d = 1 / d;
        h *= d * c;
        aa = -(a + m) * (qab + m) * x / ((a + m2) * (qap + m2));
        // Next step of the recurrence (the odd one)
        d = 1 + aa * d;
        if (Math.abs(d) < fpmin) {
            d = fpmin;
        }
        c = 1 + aa / c;
        if (Math.abs(c) < fpmin) {
            c = fpmin;
        }
        d = 1 / d;
        let del = d * c;
        h *= del;
        if (Math.abs(del - 1.0) < 3e-7) {
            break;
        }
    }
    return h;
}
exports.betacf = betacf;
;
// Returns the inverse of the incomplete beta function
function ibetainv(p, a, b) {
    const EPS = 1e-8;
    let a1 = a - 1;
    let b1 = b - 1;
    let lna, lnb, pp, t, u, err, x, al, h, w;
    if (p <= 0) {
        return 0;
    }
    if (p >= 1) {
        return 1;
    }
    if (a >= 1 && b >= 1) {
        pp = (p < 0.5) ? p : 1 - p;
        t = Math.sqrt(-2 * Math.log(pp));
        x = (2.30753 + t * 0.27061) / (1 + t * (0.99229 + t * 0.04481)) - t;
        if (p < 0.5) {
            x = -x;
        }
        al = (x * x - 3) / 6;
        h = 2 / (1 / (2 * a - 1) + 1 / (2 * b - 1));
        w = (x * Math.sqrt(al + h) / h) - (1 / (2 * b - 1) - 1 / (2 * a - 1)) *
            (al + 5 / 6 - 2 / (3 * h));
        x = a / (a + b * Math.exp(2 * w));
    }
    else {
        lna = Math.log(a / (a + b));
        lnb = Math.log(b / (a + b));
        t = Math.exp(a * lna) / a;
        u = Math.exp(b * lnb) / b;
        w = t + u;
        if (p < t / w) {
            x = Math.pow(a * w * p, 1 / a);
        }
        else {
            x = 1 - Math.pow(b * w * (1 - p), 1 / b);
        }
    }
    const afac = -(0, func_gamma_js_1.gammaln)(a) - (0, func_gamma_js_1.gammaln)(b) + (0, func_gamma_js_1.gammaln)(a + b);
    for (let j = 0; j < 10; j++) {
        if (x === 0 || x === 1) {
            return x;
        }
        err = ibeta(x, a, b) - p;
        t = Math.exp(a1 * Math.log(x) + b1 * Math.log(1 - x) + afac);
        u = err / t;
        x -= (t = u / (1 - 0.5 * Math.min(1, u * (a1 / x - b1 / (1 - x)))));
        if (x <= 0) {
            x = 0.5 * (x + t);
        }
        if (x >= 1) {
            x = 0.5 * (x + t + 1);
        }
        if (Math.abs(t) < EPS * x && j > 0) {
            break;
        }
    }
    return x;
}
exports.ibetainv = ibetainv;
;
// Returns the incomplete beta function I_x(a,b)
function ibeta(x, a, b) {
    // Factors in front of the continued fraction.
    const bt = (x === 0 || x === 1) ? 0 :
        Math.exp((0, func_gamma_js_1.gammaln)(a + b) - (0, func_gamma_js_1.gammaln)(a) - (0, func_gamma_js_1.gammaln)(b) +
            a * Math.log(x) + b * Math.log(1 - x));
    if (x < 0 || x > 1)
        // return false;
        return 0;
    if (x < (a + 1) / (a + b + 2)) {
        // Use continued fraction directly.
        return bt * betacf(x, a, b) / a;
    }
    else {
        // else use continued fraction after making the symmetry transformation.
        return 1 - bt * betacf(1 - x, b, a) / b;
    }
}
exports.ibeta = ibeta;
;
//# sourceMappingURL=func_beta.js.map