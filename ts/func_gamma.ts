// Log-gamma function
export function gammaln(x: number): number {
    const cof = [
        76.18009172947146,
        -86.50532032941677,
        24.01409824083091,
        -1.231739572450155,
        0.1208650973866179e-2,
        -0.5395239384953e-5
    ];

    const xx = x;
    let ser = 1.000000000190015;
    let tmp = x + 5.5;
    tmp -= (xx + 0.5) * Math.log(tmp);

    for (let j = 0; j < 6; j++) {
        x++
        ser += cof[j] / x;
    }
    return Math.log(2.5066282746310005 * ser / xx) - tmp;
};

// log-gamma function to support poisson distribution sampling.
// The algorithm comes from SPECFUN by Shanjie Zhang and Jianming Jin and
// their book "Computation of Special Functions", 1996, John Wiley & Sons, Inc.
export function loggam(x: number): number {
  const a = [
      8.333333333333333e-02,
      -2.777777777777778e-03,
      7.936507936507937e-04,
      -5.952380952380952e-04,
      8.417508417508418e-04,
      -1.917526917526918e-03,
      6.410256410256410e-03,
      -2.955065359477124e-02,
      1.796443723688307e-01,
      -1.39243221690590e+00
  ];

  let x0 = x;
  let n = 0;

  if ((x == 1.0) || (x == 2.0)) {
      return 0.0;
  }
  if (x <= 7.0) {
      n = Math.floor(7 - x);
      x0 = x + n;
  }

  let x2 = 1.0 / (x0 * x0);
  let xp = 2 * Math.PI;
  let gl0 = a[9];

  for (let k = 8; k >= 0; k--) {
      gl0 *= x2;
      gl0 += a[k];
  }

  let gl = gl0 / x0 + 0.5 * Math.log(xp) + (x0 - 0.5) * Math.log(x0) - x0;

  if (x <= 7.0) {
      for (let k = 1; k <= n; k++) {
          gl -= Math.log(x0 - 1.0);
          x0 -= 1.0;
      }
  }
  return gl;
}

// gamma of x
export function gammafn(x: number): number {
    const p = [
        -1.716185138865495,
        24.76565080557592,
        -379.80425647094563,
        629.3311553128184,
        866.9662027904133,
        -31451.272968848367,
        -36144.413418691176,
        66456.14382024054
    ];
    const q = [
        -30.8402300119739,
        315.35062697960416,
        -1015.1563674902192,
        -3107.771671572311,
        22538.118420980151,
        4755.8462775278811,
        -134659.9598649693,
        -115132.2596755535
    ];

    let fact = 0;
    let n = 0;
    let xden = 0;
    let xnum = 0;
    let y = x;
    let res, z;

    if (x > 171.6243769536076) {
        return Infinity;
    }

    if (y <= 0) {
        res = y % 1 + 3.6e-16;
        if (res) {
            fact = (!(y & 1) ? 1 : -1) * Math.PI / Math.sin(Math.PI * res);
            y = 1 - y;
        } else {
            return Infinity;
        }
    }

    let yi = y;
    if (y < 1) {
        z = y++;
    } else {
        z = (y -= n = (y | 0) - 1) - 1;
    }

    for (let i = 0; i < 8; i++) {
        xnum = (xnum + p[i]) * z;
        xden = xden * z + q[i];
    }
    res = xnum / xden + 1;

    if (yi < y) {
        res /= yi;
    } else if (yi > y) {
        for (let i = 0; i < n; i++) {
            res *= y;
            y++;
        }
    }
    if (fact) {
        res = fact / res;
    }
    return res;
};


// lower incomplete gamma function, which is usually typeset with a
// lower-case greek gamma as the function symbol
export function gammap(a: number, x:number): number {
  return lowRegGamma(a, x) * gammafn(a);
};


// The lower regularized incomplete gamma function, usually written P(a,x)
export function lowRegGamma(a: number, x:number): number {
  const aln = gammaln(a);
  let ap = a;
  let sum = 1 / a;
  let del = sum;
  let b = x + 1 - a;
  let c = 1 / 1.0e-30;
  let d = 1 / b;
  let h = d;
  // let i = 1;

  // calculate maximum number of itterations required for a
  let ITMAX = -~(Math.log((a >= 1) ? a : 1 / a) * 8.5 + a * 0.4 + 17);
  let an;

  if (x < 0 || a <= 0) {
    return NaN;
  } else if (x < a + 1) {
    for (let i = 1; i <= ITMAX; i++) {
      sum += del *= x / ++ap;
    }
    return (sum * Math.exp(-x + a * Math.log(x) - (aln)));
  }

  for (let i = 1; i <= ITMAX; i++) {
    an = -i * (i - a);
    b += 2;
    d = an * d + b;
    c = b + an / c;
    d = 1 / d;
    h *= d * c;
  }

  return (1 - h * Math.exp(-x + a * Math.log(x) - (aln)));
};

// Returns the inverse of the lower regularized inomplete gamma function
export function gammapinv(p:number, a:number): number {
    // var j = 0;
    let a1 = a - 1;
    let EPS = 1e-8;
    let gln = gammaln(a);
    let x, err, t, u, pp;
    let afac = 0;
    let lna1 = 0;

    if (p >= 1) {
        return Math.max(100, a + 100 * Math.sqrt(a));
    }
    if (p <= 0) {
        return 0;
    }
    if (a > 1) {
        lna1 = Math.log(a1);
        afac = Math.exp(a1 * (lna1 - 1) - gln);
        pp = (p < 0.5) ? p : 1 - p;
        t = Math.sqrt(-2 * Math.log(pp));
        x = (2.30753 + t * 0.27061) / (1 + t * (0.99229 + t * 0.04481)) - t;
        if (p < 0.5)
            x = -x;
        x = Math.max(1e-3,
                     a * Math.pow(1 - 1 / (9 * a) - x / (3 * Math.sqrt(a)), 3));
    } else {
        t = 1 - a * (0.253 + a * 0.12);
        if (p < t)
            x = Math.pow(p / t, 1 / a);
        else
            x = 1 - Math.log(1 - (p - t) / (1 - t));
    }

    for (let j = 0; j < 12; j++) {
        if (x <= 0)
            return 0;
        err = lowRegGamma(a, x) - p;
        if (a > 1) {
            t = afac * Math.exp(-(x - a1) + a1 * (Math.log(x) - lna1));
        } else {
            t = Math.exp(-x + a1 * Math.log(x) - gln);
        }
        u = err / t;
        x -= (t = u / (1 - 0.5 * Math.min(1, u * ((a - 1) / x - 1))));
        if (x <= 0)
            x = 0.5 * (x + t);
        if (Math.abs(t) < EPS * x)
            break;
    }

    return x;
};

